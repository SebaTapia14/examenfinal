from django.shortcuts import render
from tienda_m.models import Registro

def load_registro(request):
    print('mantenedorregistro.py -> load_registro')
    print('method -> ', request.method)
    if request.method == 'GET':
        try:
            codigo = request.GET['codigo']
            print('codigo -> ', codigo)
            registro = Registro.objects.get(pk=codigo)
            registro.delete()
        except Exception as e:
            print(e)
    if request.method == 'POST':
        try:
            #Lectura Formulario
            id = request.POST['codigo']
            email = request.POST['email']
            contraseña = request.POST['contraseña']
            rcontraseña = request.POST['rcontraseña']
            nombres = request.POST['nombres']
            apellidos = request.POST['apellidos']            
            dia_nacimiento = request.POST['dia_nacimiento']
            mes_nacimiento = request.POST['mes_nacimiento']
            anio_nacimiento = request.POST['anio_nacimiento']
            region = request.POST['region']
            comuna = request.POST['comuna']
            ciudad = request.POST['ciudad']
            #Busqueda de objecto en la base de datos
            registro = Registro.objects.get(pk=id)
            #Actualizando objeto en memoria volatil
            registro.email = email
            registro.contraseña = contraseña
            registro.rcontraseña = rcontraseña            
            registro.nombres = nombres            
            registro.apellidos = apellidos            
            registro.dia_nacimiento = dia_nacimiento
            registro.mes_nacimiento = mes_nacimiento
            registro.anio_nacimiento = anio_nacimiento
            registro.region = region
            registro.comuna = comuna
            registro.ciudad = ciudad  
            #Actualizando en la base de datos                      
            registro.save(force_update=True)
        except Exception as e:
            print(e)
    registros = Registro.objects.all
    return render(request, 'mantenedor-registro.html', {'registros': registros})
