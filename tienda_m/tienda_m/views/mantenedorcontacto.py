from django.shortcuts import render, redirect
from tienda_m.models import Contacto
from tienda_m.views.login import authorization

def load_contacto(request):
    print('mantenedorcontacto.py -> load_contacto')    
    print('method -> ', request.method)
    if request.method == 'GET':
        try:
            auth, error = authorization(request, 'delete_contacto')
            if not auth:
                redirect(error)
            codigo = request.GET['codigo']
            print('codigo -> ', codigo)
            contacto = Contacto.objects.get(pk=codigo)
            contacto.delete()
        except Exception as e:
            print(e)
    if request.method == 'POST':
        try:
            #Lectura Formulario
            id = request.POST['codigo']
            nombres = request.POST['nombres']
            apellido_paterno = request.POST['apellido_paterno']
            apellido_materno = request.POST['apellido_materno']
            email = request.POST['email']
            telefono = request.POST['telefono']            
            asunto = request.POST['asunto']
            #Busqueda de objecto en la base de datos
            contacto = Contacto.objects.get(pk=id)
            #Actualizando objeto en memoria volatil
            contacto.nombres = nombres
            contacto.apellido_paterno = apellido_paterno
            contacto.apellido_materno = apellido_materno            
            contacto.email = email            
            contacto.telefono = telefono            
            contacto.asunto = asunto  
            #Actualizando en la base de datos                      
            contacto.save(force_update=True)
        except Exception as e:
            print(e)
    contactos = Contacto.objects.all
    return render(request, 'mantenedor-contacto.html', {'contactos': contactos})            
        

