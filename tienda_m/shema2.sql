CREATE TABLE IF NOT EXISTS public.registro 
(
	codigo bigserial,
	email character varying(80) COLLATE pg_catalog."default",
    "contraseña" character varying(80) COLLATE pg_catalog."default",
    "rcontraseña" character varying(80) COLLATE pg_catalog."default",
    nombres character varying(80) COLLATE pg_catalog."default",
    apellidos character varying(80) COLLATE pg_catalog."default",
    run numeric(8,0),
    dv character varying(1) COLLATE pg_catalog."default",
    dia_nacimiento character varying(40) COLLATE pg_catalog."default",
    mes_nacimiento character varying(40) COLLATE pg_catalog."default",
    anio_nacimiento character varying(40) COLLATE pg_catalog."default",
    region character varying(80) COLLATE pg_catalog."default",
    comuna character varying(80) COLLATE pg_catalog."default",
    ciudad character varying(80) COLLATE pg_catalog."default",
	CONSTRAINT registro_pkey primary key (codigo)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.registro
OWNER to postgres;

